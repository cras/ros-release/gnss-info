## gnss-info (noetic) - 1.0.2-1

The packages in the `gnss-info` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic gnss-info` on `Tue, 27 Feb 2024 10:34:15 -0000`

These packages were released:
- `gnss_info`
- `gnss_info_msgs`
- `gnsstk_ros`

Version of package(s) in repository `gnss-info`:

- upstream repository: https://github.com/ctu-vras/gnss-info.git
- release repository: https://gitlab.fel.cvut.cz/cras/ros-release/gnss-info.git
- rosdistro version: `1.0.1-1`
- old version: `1.0.1-1`
- new version: `1.0.2-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


## gnss-info (noetic) - 1.0.1-1

The packages in the `gnss-info` repository were released into the `noetic` distro by running `/usr/bin/bloom-release -r noetic --new-track gnss-info` on `Tue, 14 Nov 2023 17:34:29 -0000`

These packages were released:
- `gnss_info`
- `gnss_info_msgs`
- `gnsstk_ros`

Version of package(s) in repository `gnss-info`:

- upstream repository: https://github.com/ctu-vras/gnss-info.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `1.0.1-1`

Versions of tools used:

- bloom version: `0.11.2`
- catkin_pkg version: `1.0.0`
- rosdep version: `0.22.2`
- rosdistro version: `0.9.0`
- vcstools version: `0.1.42`


